import Vue from 'vue'
import Router from 'vue-router'

import Hello from '@/components/Hello'
import Quiz from '@/components/Quiz'
import Success from '@/components/Success'
import Gameover from '@/components/GameOver'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    }, {
      path: '/quiz',
      name: 'Quiz',
      component: Quiz
    }, {
      path: '/success',
      name: 'Success',
      component: Success
    }, {
      path: '/gameover',
      name: 'Gameover',
      component: Gameover
    }
  ]
})
